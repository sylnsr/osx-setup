#!/usr/bin/env python
import datetime as dt
from subprocess import call
import shlex

currentHour = dt.datetime.now().hour

if currentHour > 20 or currentHour < 8:
    call(shlex.split('sudo tmutil setdestination /Volumes/Transcend'))
else:
    call(shlex.split('sudo tmutil setdestination /Volumes/Backup'))